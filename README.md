# paludis

Ansible module for installing packages with `Paludis` package manager.

## Usage

1. Clone this repository to `library` subdirectory inside role or directory containing playbooks.
2. Add `library = library` to `[defaults]` section of `ansible.cfg`.
3. Check that module is available now: `ansible-doc paludis`.
 
Refer to that documentation for using `paludis` in playbooks.
