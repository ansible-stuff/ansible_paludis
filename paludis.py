#!/usr/bin/python3
"""Ansible plugin for Paludis package manager."""

# Copyright: (c) 2019, Alexander Kapshuna <kapsh@kap.sh>
# GNU General Public License v3.0+ (see COPYING or https://www.gnu.org/licenses/gpl-3.0.txt)

ANSIBLE_METADATA = {
    "metadata_version": "1.1",
    "status": ["preview"],
    "supported_by": "community",
}

DOCUMENTATION = """
---
module: paludis

short_description: Install or remove packages with paludis

version_added: "2.8"

description:
  - Install or remove packages with paludis.

options:
  name:
    description:
      - Package name (with category)
    required: true
  state:
    description:
      - Whether to install (I(present), I(latest)) or remove (I(absent)) a package.
    choices: 
      - absent
      - latest
      - present
    default: present

requirements:
  - C(cave) command
  - Python 3.6 or newer

author:
  - Alexander Kapshuna (@kapsh)
"""

EXAMPLES = """
# Make sure package foo is installed
- paludis:
    name: foo
    state: present
"""

RETURN = """
packages:
    description: State of requested package(s)
    type: dict
    returned: always
"""

from enum import Enum
from typing import Dict, List

from ansible.module_utils.basic import AnsibleModule


class State(Enum):
    """Possible package states."""

    absent = "absent"
    latest = "latest"
    present = "present"


class CaveModule(AnsibleModule):
    """Work with Paludis package manager using cave client."""

    def __init__(self):
        module_args = {
            "name": {"type": "list", "required": True},
            "state": {
                "type": "str",
                "default": State.present.value,
                "choices": [s.value for s in State],
            },
        }
        super().__init__(module_args, supports_check_mode=True)

        self.cave = self.get_bin_path("cave", required=True)

    def run(self):
        """Module entry-point."""
        params = self.params
        result = {"changed": False, "packages": {}}

        packages = params["name"]
        desired_state = State(params["state"])

        # TODO normalise names to full form?
        current = self.current_state(packages)

        diff = [
            f"!{name}" if desired_state == State.absent else name
            for name in packages
            if current[name] != desired_state
        ]

        result["changed"] = bool(diff)

        # TODO compare states before check
        if self.check_mode:
            result["packages"] = readable_state(current)
            self.exit_json(**result)

        if diff:
            self.install_packages(diff)

        # TODO check if all requested was installed
        result["packages"] = readable_state(self.current_state(packages))

        self.exit_json(**result)

    def current_state(self, names: List[str]) -> Dict[str, State]:
        """Query state for each package in list."""
        return {name: self.query_package(name) for name in names}

    def query_package(self, name: str) -> State:
        """Get the State for given package."""
        # TODO hardcode installed for now
        cmd = (
            f"{self.cave} print-ids -m {name} -s uninstall -m */*::installed -f '%u\n'"
        )
        _, installed, _ = self.run_command(cmd, check_rc=True)
        if name not in installed:
            return State.absent

        cmd = f"{self.cave} find-candidates -v -m {name}"
        _, candidate, _ = self.run_command(cmd, check_rc=True)
        return State.latest if installed == candidate else State.present

    def install_packages(self, names: List[str]) -> None:
        """Install list of packages."""
        cmd = f"{self.cave} resolve -zx " + " ".join(names)
        self.run_command(cmd, check_rc=True)


def readable_state(packages_state: Dict[str, State]) -> Dict[str, str]:
    """Get JSON serializable form of state mapping."""
    return {name: state.value for name, state in packages_state.items()}


def main():
    """Entry point."""
    CaveModule().run()


if __name__ == "__main__":
    main()
